# Dev environment setup

```bash
$ pipenv install -e . --dev
```

# Tests

```bash
$ pipenv run python runtests.py
```
