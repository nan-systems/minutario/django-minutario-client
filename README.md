# Minutario Client

Quick start
-----------

1. Add `django_minutario_client` to your INSTALLED_APPS setting like this. It's very important that `django_minutario_client` comes before `rest_framework`, otherwise it will not use it's custom field templates.
```
    INSTALLED_APPS = [
        ...
        'django_minutario_client',
        'rest_framework',
    ]
```

2. Configure minutario client setings:
```
    # The base URL where the API is
    MINUTARIO_URL = "http://localhost:8000"
    # The auth credentials for this user
    MINUTARIO_AUTH = ("test", "test")
    # Define which serializer should be used for each template slug
    DOCUMENT_SERIALIZER_CLASSES = {"template-slug": "app.serializers.YourSerializer"}
```

3. Include the minutario client URLconf in your project urls.py like this:
```
    path('minutario/', include('django_minutario_client.urls')),
```

4. (Optional) Make the template list view the root url:
```
    path("", RedirectView.as_view(pattern_name="template_list")),
```

Install from Gitlab
-------------------

```
pipenv install -e "git+ssh://git@gitlab.com/nan-systems/minutario/django-minutario-client#egg=django_minutario_client"
```


Local development
-----------------

To easy local development we recommend to install the library directly, instead of getting it from gitlab.
To do that in pipenv you should add this to the Pipfile of the project that will use it.

```
django-minutario-client = {editable = true, path = "./../django-minutario-client"}
```
