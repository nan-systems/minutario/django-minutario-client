import os

from django.conf import settings

APP_DIR = os.path.abspath(os.path.dirname(__file__))


# Minimum settings required for the app's tests.
SETTINGS_DICT = {
    "SECRET_KEY": 'secret',
    "BASE_DIR": APP_DIR,
    "INSTALLED_APPS": (
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.sites",
        "rest_framework",
        "django_minutario_client",
        "tests",
    ),
    # Test cases will override this liberally.
    "ROOT_URLCONF": "django_minutario_client.urls",
    "DATABASES": {
        "default": {"ENGINE": "django.db.backends.sqlite3", "NAME": ":memory:"}
    },
    "MIDDLEWARE": (
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
    ),
    "TEMPLATES": [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": [
                os.path.join(APP_DIR, "django_minutario_client/templates"),
                os.path.join(APP_DIR, "tests/templates"),
            ],
            "OPTIONS": {
                "context_processors": [
                    "django.contrib.auth.context_processors.auth",
                    "django.template.context_processors.debug",
                    "django.template.context_processors.i18n",
                    "django.template.context_processors.media",
                    "django.template.context_processors.static",
                    "django.template.context_processors.tz",
                    "django.contrib.messages.context_processors.messages",
                ]
            },
        }
    ],
    "MINUTARIO_URL": "http://localhost:8000",
    "MINUTARIO_AUTH": ("test", "test"),
    "PASSWORD_HASHERS": ["django.contrib.auth.hashers.MD5PasswordHasher"],
}


def pytest_configure():
    settings.configure(**SETTINGS_DICT)
