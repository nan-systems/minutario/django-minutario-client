from django.apps import AppConfig


class MinutarioClientConfig(AppConfig):
    name = "django_minutario_client"
