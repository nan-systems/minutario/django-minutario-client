import re

from rest_framework import serializers
from rest_framework.fields import JSONField, empty
from rest_framework.utils import html

from flatten_json import unflatten_list
from rest_framework.utils.serializer_helpers import (
    BoundField,
    JSONBoundField,
    NestedBoundField,
)


class NestedListBoundField(BoundField):
    def __iter__(self):
        repetitions = max(len(self.value), 1)

        for i in range(repetitions):
            yield self[i]

    def __getitem__(self, key):
        if not isinstance(key, int):
            return super().__getitem__(key)

        field = self.child
        value = self.value[key] if self.value else field.get_initial()
        error = self.errors[key] if isinstance(self.errors, list) else None
        return NestedBoundField(field, value, error, prefix=f"{self.name}[{key}]")


class NestlingListSerializer(serializers.ListSerializer):
    def get_value(self, dictionary):
        """
        Given the input dictionary, return the field value.
        """
        # We override the default field access in order to support
        # lists in HTML forms.
        if html.is_html_input(dictionary):
            return parse_html_dict(dictionary, prefix=self.field_name)

        return dictionary.get(self.field_name, empty)


class NestlingSerializer(serializers.Serializer):
    def get_value(self, dictionary):
        # We override the default field access in order to support
        # nested HTML forms.
        if html.is_html_input(dictionary):
            return parse_html_dict(dictionary, prefix=self.field_name) or empty
        return dictionary.get(self.field_name, empty)

    def __getitem__(self, key):
        field = self.fields[key]
        value = self.data.get(key)
        error = self.errors.get(key) if hasattr(self, "_errors") else None
        if isinstance(field, serializers.Serializer):
            return NestedBoundField(field, value, error)
        if isinstance(field, JSONField):
            return JSONBoundField(field, value, error)
        if isinstance(field, serializers.ListSerializer):
            return NestedListBoundField(field, value, error)
        return BoundField(field, value, error)

    class Meta:
        list_serializer_class = NestlingListSerializer


def parse_html_dict(query_data, prefix):
    separator = "."
    new_dict = {}

    for key in query_data:
        is_unindexed_list = key.endswith("[]")
        is_indexed_list = re.search(r"\[\d+\]", key)

        if is_unindexed_list:
            value = query_data.getlist(key)
            new_key = key.removesuffix("[]")
            for index, item in enumerate(value):
                new_dict[f"{new_key}{separator}{index}"] = item
        elif is_indexed_list:
            value = query_data[key]
            new_key = key.replace("[", ".").replace("]", "")
            new_dict[new_key] = value
        else:
            new_dict[key] = query_data[key]

    unflatted = unflatten_list(new_dict, separator=separator).get(prefix)
    return unflatted
