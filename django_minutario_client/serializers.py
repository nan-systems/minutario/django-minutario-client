from django.utils.translation import gettext_lazy as _

from rest_framework import serializers
from rest_framework.fields import empty
from rest_framework.utils import html

from django_minutario_client.nestling_serializers import parse_html_dict


STATE_CHOICES = (
    ("AC", "Acre"),
    ("AL", "Alagoas"),
    ("AP", "Amapá"),
    ("AM", "Amazonas"),
    ("BA", "Bahia"),
    ("CE", "Ceará"),
    ("DF", "Distrito Federal"),
    ("ES", "Espírito Santo"),
    ("GO", "Goiás"),
    ("MA", "Maranhão"),
    ("MT", "Mato Grosso"),
    ("MS", "Mato Grosso do Sul"),
    ("MG", "Minas Gerais"),
    ("PA", "Pará"),
    ("PB", "Paraíba"),
    ("PR", "Paraná"),
    ("PE", "Pernambuco"),
    ("PI", "Piauí"),
    ("RJ", "Rio de Janeiro"),
    ("RN", "Rio Grande do Norte"),
    ("RS", "Rio Grande do Sul"),
    ("RO", "Rondônia"),
    ("RR", "Roraima"),
    ("SC", "Santa Catarina"),
    ("SP", "São Paulo"),
    ("SE", "Sergipe"),
    ("TO", "Tocantins"),
)


class BrazilianState:
    def __init__(self, sigla, nome):
        self.sigla = sigla
        self.nome = nome

    def __str__(self):
        return self.sigla


class DetailedBrazilianStateField(serializers.ChoiceField):
    def __init__(self, choices=STATE_CHOICES, **kwargs):
        super().__init__(choices=choices, **kwargs)

    def to_internal_value(self, data):
        initials = super().to_internal_value(data)
        name = self.grouped_choices[initials]
        return BrazilianState(sigla=initials, nome=name)


class BrazilianAddressSerializer(serializers.Serializer):
    cep = serializers.CharField(label="CEP", required=False)
    logradouro = serializers.CharField(label="Logradouro", required=False)
    numero = serializers.CharField(label="Número", required=False)
    complemento = serializers.CharField(label="Complemento", required=False)
    bairro = serializers.CharField(label="Bairro", required=False)
    cidade = serializers.CharField(label="Cidade", required=False)
    estado = serializers.ChoiceField(
        label="Estado", choices=STATE_CHOICES, required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style = {"template": "brazilian_address.html", **self.style}


class OrderedMultipleChoiceField(serializers.MultipleChoiceField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style = {"template": "ordered_multiple_checkbox.html", **self.style}

    def to_representation(self, value):
        return [
            self.choice_strings_to_values.get(str(item), item)
            for item in self.choices
            if item in value
        ]

    def get_value(self, dictionary):
        # We override the default field access in order to support
        # nested HTML forms.
        if html.is_html_input(dictionary):
            return parse_html_dict(dictionary, prefix=self.field_name) or empty
        return dictionary.get(self.field_name, empty)
