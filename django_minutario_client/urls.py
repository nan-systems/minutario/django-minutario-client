from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from django_minutario_client import views

urlpatterns = [
    path("", RedirectView.as_view(url=reverse_lazy("template_list"))),
    path(
        "templates/",
        views.TemplateListView.as_view(),
        name="template_list",
    ),
    path(
        "templates/<slug>/",
        views.TemplateDetailView.as_view(),
        name="template_detail",
    ),
    path(
        "document/<pk>/download/",
        views.DocumentDownloadView.as_view(),
        name="document_download",
    ),
    path(
        "document/<template_slug>/new/",
        views.DocumentCreateView.as_view(),
        name="document_new",
    ),
]
