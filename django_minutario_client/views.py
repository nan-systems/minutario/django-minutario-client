import io
import json
import re

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import redirect_to_login
from django.urls import reverse
from django.views.generic import TemplateView, View
from django.http import FileResponse, HttpResponseRedirect
from django.http.request import QueryDict
from django.utils.module_loading import import_string
from rest_framework.utils.encoders import JSONEncoder as DRFJSONEncoder

from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.views import APIView, exception_handler
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.permissions import IsAuthenticated

import slumber


class TemplateListView(LoginRequiredMixin, TemplateView):
    template_name = "django_minutario_client/template_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        auth = tuple(settings.MINUTARIO_AUTH)
        api = slumber.API(settings.MINUTARIO_URL, auth=auth)
        templates = api.templates.get()
        context["templates"] = templates
        return context


class TemplateDetailView(LoginRequiredMixin, TemplateView):
    template_name = "django_minutario_client/template_detail.html"

    def build_pagination_uri(self, request_uri, pagination_uri):
        if pagination_uri and "?" in pagination_uri:
            params = QueryDict(pagination_uri.split("?", 1)[-1]).urlencode()
            return f"{request_uri.split('?', 1)[0]}?{params}"
        else:
            return request_uri

    def get_page_size(self, params):
        page_size = 0
        if hasattr(settings, "REST_FRAMEWORK"):
            page_size = settings.REST_FRAMEWORK.get("PAGE_SIZE", 0)
        return int(params.get("limit", page_size))

    def get_context_data(self, slug, **kwargs):
        context = super().get_context_data(**kwargs)
        auth = tuple(settings.MINUTARIO_AUTH)
        api = slumber.API(settings.MINUTARIO_URL, auth=auth)

        request_uri = self.request.build_absolute_uri()
        params = self.request.GET
        page_size = limit = self.get_page_size(params)
        offset = int(params.get("offset", 0))

        if not page_size:
            documents = api.documents.get(template_version__template__slug=slug)
            context["documents"] = documents
            context["slug"] = slug
            return context

        documents = api.documents.get(
            template_version__template__slug=slug, limit=limit, offset=offset
        )

        context.update(
            dict(
                documents=documents["results"],
                slug=slug,
                count=int(documents["count"]),
                start=offset + 1,
                end=min(offset + limit, int(documents["count"])),
                next=self.build_pagination_uri(request_uri, documents["next"]),
                previous=self.build_pagination_uri(request_uri, documents["previous"]),
            )
        )

        return context


class DocumentDownloadView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        auth = tuple(settings.MINUTARIO_AUTH)
        api = slumber.API(settings.MINUTARIO_URL, auth=auth)
        format_ = self.request.GET.get("format", "docx")
        document_url = api.documents(kwargs.get("pk")).download.url()
        document = api._store["session"].get(document_url, params={"format": format_})

        content_dispostion = document.headers.get("content-disposition", "")
        filename = re.findall("filename=(.+)", content_dispostion)
        if len(filename) > 0:
            filename = filename[0]
        else:
            filename = f"document.{format_}"

        return FileResponse(io.BytesIO(document.content), filename=filename)


class DRFJsonSerializer(slumber.serialize.JsonSerializer):
    def dumps(self, data):
        return json.dumps(data, cls=DRFJSONEncoder)


class Minutario:
    def __init__(self, auth):
        serializer = slumber.serialize.Serializer(
            default="json", serializers=[DRFJsonSerializer()]
        )
        self.api = slumber.API(settings.MINUTARIO_URL, auth=auth, serializer=serializer)

    def create_document(self, template_slug, data):
        template = self.api.templates(template_slug).get()
        last_template_version = template["template_versions"][-1]
        return self.api.documents.post(
            {
                "template_version": last_template_version["pk"],
                "data": data,
            }
        )


def hybrid_exception_handler(exc, context):
    request = context["request"]
    if request.accepted_media_type == "text/html":
        if isinstance(exc, exceptions.NotAuthenticated):
            next_url = request.get_full_path()
            return redirect_to_login(next=next_url)

    return exception_handler(exc, context)


class DocumentCreateView(APIView):
    template_name = "django_minutario_client/document_form.html"
    renderer_classes = [TemplateHTMLRenderer]
    permission_classes = (IsAuthenticated,)

    def get_exception_handler(self):
        return hybrid_exception_handler

    def get(self, request, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class()
        context = {
            "serializer": serializer,
            "template_slug": kwargs["template_slug"],
        }
        return Response(context)

    def post(self, request, template_slug, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data)

        if serializer.is_valid():
            auth = tuple(settings.MINUTARIO_AUTH)
            minutario = Minutario(auth)
            minutario.create_document(template_slug, serializer.data)
            return HttpResponseRedirect(self.get_success_url())
        else:
            context = {
                "serializer": serializer,
                "template_slug": template_slug,
            }
            return Response(context)
        return Response(context)

    def get_serializer_class(self):
        serializer_class = import_string(
            settings.DOCUMENT_SERIALIZER_CLASSES[self.kwargs["template_slug"]]
        )
        return serializer_class

    def get_success_url(self):
        return reverse("template_detail", kwargs={"slug": self.kwargs["template_slug"]})
