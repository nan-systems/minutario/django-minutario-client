from os.path import dirname, join

import setuptools


setuptools.setup(
    name="django-minutario-client",
    version="0.11.4",
    packages=["django_minutario_client"],
    include_package_data=True,
    install_requires=["slumber", "djangorestframework", "flatten_json"],
    description="Django app that integrates with minutario backend",
    long_description=open(join(dirname(__file__), "README.md")).read(),
    long_description_content_type="text/markdown",
    author="Thiago Garcia da Silva",
    author_email="thiagogds14@gmail.com",
    license="BSD-3-Clause",
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 3.0",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
    ],
)
