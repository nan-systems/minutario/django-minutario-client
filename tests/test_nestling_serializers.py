from collections import OrderedDict
from django.http.request import QueryDict
from rest_framework import serializers
from rest_framework.exceptions import ErrorDetail

from django_minutario_client.serializers import OrderedMultipleChoiceField
from django_minutario_client.nestling_serializers import (
    NestlingSerializer,
    parse_html_dict,
)


required_field_error = ErrorDetail(string="This field is required.", code="required")


class TestParseSingleDepthHtmlDict:
    def test_without_unrelated_fields(self):
        username = "example"
        email = "example@example.com"
        query_data = QueryDict(f"profile.username={username}&profile.email={email}")

        parsed = parse_html_dict(query_data, prefix="profile")
        assert username == parsed["username"]
        assert email == parsed["email"]
        assert 2 == len(parsed.keys())

    def test_unrelated_field(self):
        username = "example"
        email = "example@example.com"
        query_data = QueryDict(
            f"profile.username={username}&profile.email={email}&unrelated=42"
        )

        parsed = parse_html_dict(query_data, prefix="profile")
        assert username == parsed["username"]
        assert email == parsed["email"]
        assert 2 == len(parsed.keys())

    def test_repeated_fields(self):
        username = "example"
        username_2 = "example_2"
        query_data = QueryDict(
            f"profile.username={username}&profile.username={username_2}"
        )

        parsed = parse_html_dict(query_data, prefix="profile")
        assert username, username_2 == parsed["username"]
        assert 1 == len(parsed.keys())

    def test_non_required_nested_field(self):
        quantity = 7
        query_data = QueryDict(f"other_prefix.quantity={quantity}")

        parsed = parse_html_dict(query_data, prefix="prefix")
        assert parsed is None

    def test_parse_unordered_list(self):
        query_data = QueryDict(f"prefix.choices[]=choice_1&prefix.choices[]=choice_2")

        parsed = parse_html_dict(query_data, prefix="prefix")
        assert dict(choices=["choice_1", "choice_2"]) == parsed

    def test_parse_ordered_list(self):
        query_data = QueryDict(f"company[0].name=name_1&company[1].name=name_2&")

        parsed = parse_html_dict(query_data, prefix="company")
        assert [dict(name="name_1"), dict(name="name_2")] == parsed


class TestParseMultiDepthHtmlDict:
    def test_without_unrelated_fields(self):
        username = "example"
        email = "example@example.com"
        query_data = QueryDict(
            f"user.profile.username={username}&user.profile.email={email}"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        assert profile == parsed["profile"]
        assert 1 == len(parsed.keys())

    def test_with_unrelated_fields(self):
        username = "example"
        email = "example@example.com"
        query_data = QueryDict(
            f"user.profile.username={username}&user.profile.email={email}&unrelated=42"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        assert profile == parsed["profile"]
        assert 1 == len(parsed.keys())

    def test_with_flat_field_at_the_beginning(self):
        username = "example"
        email = "example@example.com"
        ident = 42
        query_data = QueryDict(
            f"user.ident={ident}&"
            f"user.profile.username={username}&user.profile.email={email}"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        assert profile == parsed["profile"]
        assert str(ident) == parsed["ident"]
        assert 2 == len(parsed.keys())

    def test_with_flat_field_at_the_end(self):
        username = "example"
        email = "example@example.com"
        ident = 42
        query_data = QueryDict(
            f"user.profile.username={username}&user.profile.email={email}&"
            f"user.ident={ident}"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        assert profile == parsed["profile"]
        assert str(ident) == parsed["ident"]
        assert 2 == len(parsed.keys())

    def test_with_two_nested_fields(self):
        username = "example"
        email = "example@example.com"
        street = "Street name"
        number = 42
        query_data = QueryDict(
            f"user.profile.username={username}&user.profile.email={email}&"
            f"user.address.street={street}&user.address.number={number}"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        address = dict(street=street, number=str(number))
        assert profile == parsed["profile"]
        assert address == parsed["address"]
        assert 2 == len(parsed.keys())

    def test_with_two_nested_fields_out_of_order(self):
        username = "example"
        email = "example@example.com"
        street = "Street name"
        number = 42
        query_data = QueryDict(
            f"user.profile.username={username}&"
            f"user.address.street={street}&"
            f"user.profile.email={email}&"
            f"user.address.number={number}"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        address = dict(street=street, number=str(number))
        assert profile == parsed["profile"]
        assert address == parsed["address"]
        assert 2 == len(parsed.keys())

    def test_with_two_nested_fields_out_of_order_and_one_flat(self):
        username = "example"
        email = "example@example.com"
        street = "Street name"
        number = 42
        ident = 12
        query_data = QueryDict(
            f"user.profile.username={username}&"
            f"user.profile.email={email}&"
            f"user.address.street={street}&"
            f"user.address.number={number}&"
            f"user.ident={ident}"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        profile = dict(username=username, email=email)
        address = dict(street=street, number=str(number))
        assert profile == parsed["profile"]
        assert address == parsed["address"]
        assert str(ident) == parsed["ident"]
        assert 3 == len(parsed.keys())

    def test_three_levels(self):
        username = "example"
        street = "Street name"
        query_data = QueryDict(
            f"user.profile.username={username}&"
            f"user.profile.address.street={street}&"
        )

        parsed = parse_html_dict(query_data, prefix="user")
        address = dict(street=street)
        profile = dict(username=username, address=address)
        assert profile == parsed["profile"]
        assert 1 == len(parsed.keys())


class AddressSerializer(NestlingSerializer):
    street = serializers.CharField()
    number = serializers.IntegerField()


class CompanySerializer(NestlingSerializer):
    address = AddressSerializer()


class ContractSerializer(NestlingSerializer):
    company = CompanySerializer()
    quantity = serializers.IntegerField()


class MultiCompanyContractSerializer(NestlingSerializer):
    company = CompanySerializer(many=True)
    quantity = serializers.IntegerField()


class TestOrderedMultipleChoicesFieldInNestlingSerializer:
    def test_with_all_values_checked(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict(
            f"section.topics[]={topics[0]}&" f"section.topics[]={topics[1]}"
        )
        # We are using OrderedDict due to a implemetation detail of MultipleChoiceField
        expected = {"section": OrderedDict([("topics", topics)])}

        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_with_all_values_checked_unorderedly(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict(
            f"section.topics[]={topics[1]}&" f"section.topics[]={topics[0]}"
        )
        # We are using OrderedDict due to a implemetation detail of MultipleChoiceField
        expected = {"section": OrderedDict([("topics", topics)])}

        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_with_one_value_checked(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict(f"section.topics[]={topics[0]}")
        # We are using OrderedDict due to a implemetation detail of MultipleChoiceField
        expected = {"section": OrderedDict([("topics", [topics[0]])])}

        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_validate_required_when_nested(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)
            title = serializers.CharField()

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict("section.title=Title")
        serializer = ContractSerializer(data=query_data)
        assert not serializer.is_valid()

        errors = {"section": {"topics": [required_field_error]}}
        assert errors == serializer.errors


class TestMultipleChoicesFieldInNestlingSerializer:
    def test_with_all_values_checked(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = serializers.MultipleChoiceField(choices=TOPICS_CHOICES)

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict(
            f"section.topics[]={topics[0]}&" f"section.topics[]={topics[1]}"
        )
        # We are using OrderedDict due to a implemetation detail of MultipleChoiceField
        expected = {"section": OrderedDict([("topics", set(topics))])}

        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_with_one_value_checked(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = serializers.MultipleChoiceField(choices=TOPICS_CHOICES)

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict(f"section.topics[]={topics[0]}")
        # We are using OrderedDict due to a implemetation detail of MultipleChoiceField
        expected = {"section": OrderedDict([("topics", {topics[0]})])}

        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_validate_required_when_nested(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = serializers.MultipleChoiceField(choices=TOPICS_CHOICES)
            title = serializers.CharField()

        class ContractSerializer(NestlingSerializer):
            section = SectionSerializer()

        query_data = QueryDict("section.title=Title")
        serializer = ContractSerializer(data=query_data)
        assert not serializer.is_valid()

        errors = {"section": {"topics": [required_field_error]}}
        assert errors == serializer.errors


class TestNestlingSerializer:
    def test_valid(self):
        street = "Street name"
        number = 7
        quantity = 42
        query_data = QueryDict(
            f"company.address.street={street}"
            f"&company.address.number={number}&"
            f"quantity={quantity}"
        )
        expected = {
            "company": {"address": {"street": street, "number": number}},
            "quantity": quantity,
        }
        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_invalid_should_bring_filled_data_correctly(self):
        street = "Street name"
        number = 7
        query_data = QueryDict(
            f"company.address.street={street}&company.address.number={number}"
        )
        serializer = ContractSerializer(data=query_data)
        assert not serializer.is_valid()
        assert street == serializer.data["company"]["address"]["street"]
        assert str(number) == serializer.data["company"]["address"]["number"]

    def test_non_required_serializer(self):
        class ContractSerializer(NestlingSerializer):
            company = CompanySerializer(required=False)
            quantity = serializers.IntegerField()

        quantity = 7
        query_data = QueryDict(f"quantity={quantity}")

        serializer = ContractSerializer(data=query_data)
        assert serializer.is_valid()
        assert quantity == serializer.data["quantity"]
        assert None == serializer.data.get("company")

    def test_empty_serializer_for_initial_rendering(self):
        initial_value = 18

        class PersonSerializer(NestlingSerializer):
            name = serializers.CharField()
            age = serializers.IntegerField(initial=initial_value)

        person_serializer = PersonSerializer()

        name, age = list(person_serializer)
        assert initial_value == age.value
        assert "" == name.value

    def test_many_empty_serializer_for_initial_rendering(self):
        initial_value = 18

        class PersonSerializer(NestlingSerializer):
            name = serializers.CharField()
            age = serializers.IntegerField(initial=initial_value)

        class FamilySerializer(NestlingSerializer):
            people = PersonSerializer(many=True)
            pet_name = serializers.CharField()

        family_serializer = FamilySerializer()

        people, pet_name = list(family_serializer)
        assert 1 == len(list(people))

        person_serializer = people[0]

        name, age = list(person_serializer)
        assert initial_value == age.value
        assert "" == name.value


class TestMultiNestlingSerializer:
    def test_valid(self):
        street_1 = "Street one"
        number_1 = 7
        street_2 = "Street two"
        number_2 = 8
        quantity = 42
        query_data = QueryDict(
            f"company[0].address.street={street_1}&"
            f"company[0].address.number={number_1}&"
            f"company[1].address.street={street_2}&"
            f"company[1].address.number={number_2}&"
            f"quantity={quantity}"
        )
        expected = {
            "company": [
                {"address": {"street": street_1, "number": number_1}},
                {"address": {"street": street_2, "number": number_2}},
            ],
            "quantity": quantity,
        }
        serializer = MultiCompanyContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors
        assert expected == serializer.data

    def test_invalid(self):
        street_1 = "Street one"
        number_1 = "7"
        street_2 = "Street two"
        query_data = QueryDict(
            f"company[0].address.street={street_1}&"
            f"company[0].address.number={number_1}&"
            f"company[1].address.street={street_2}&"
        )
        expected = {
            "company": [
                {"address": {"street": street_1, "number": number_1}},
                {"address": {"street": street_2}},
            ],
        }

        serializer = MultiCompanyContractSerializer(data=query_data)
        assert not serializer.is_valid()
        assert expected == serializer.data

        required_field_error = ErrorDetail(
            string="This field is required.", code="required"
        )
        errors = {
            "quantity": [required_field_error],
            "company": [{}, {"address": {"number": [required_field_error]}}],
        }
        assert errors == serializer.errors


class TestNestedListBoundField:
    def test_always_has_at_least_one_child_during_iteration(self):
        serializer = MultiCompanyContractSerializer()
        companies = list(serializer["company"])
        assert 1 == len(companies)

    def test_yield_childs(self):
        street_1 = "Street one"
        number_1 = 7
        street_2 = "Street two"
        number_2 = 8
        quantity = 42
        query_data = QueryDict(
            f"company[0].address.street={street_1}&"
            f"company[0].address.number={number_1}&"
            f"company[1].address.street={street_2}&"
            f"company[1].address.number={number_2}&"
            f"quantity={quantity}"
        )
        serializer = MultiCompanyContractSerializer(data=query_data)
        assert serializer.is_valid(), serializer.errors

        companies = list(serializer["company"])
        assert 2 == len(companies)

        assert companies[0]["address"]["street"].value == street_1
        assert companies[0]["address"]["number"].value == number_1
        assert companies[0]["address"]["street"].name == "company[0].address.street"
        assert companies[0]["address"]["number"].name == "company[0].address.number"

        assert companies[1]["address"]["street"].value == street_2
        assert companies[1]["address"]["number"].value == number_2
        assert companies[1]["address"]["street"].name == "company[1].address.street"
        assert companies[1]["address"]["number"].name == "company[1].address.number"

    def test_yield_childs_with_error(self):
        street_1 = "Street one"
        number_1 = 7
        street_2 = "Street two"
        quantity = 42
        query_data = QueryDict(
            f"company[0].address.street={street_1}&"
            f"company[0].address.number={number_1}&"
            f"company[1].address.street={street_2}&"
            f"quantity={quantity}"
        )
        serializer = MultiCompanyContractSerializer(data=query_data)
        assert not serializer.is_valid()

        companies = list(serializer["company"])
        assert 2 == len(companies)

        assert companies[0]["address"]["street"].value == street_1
        assert companies[0]["address"]["number"].value == str(number_1)
        assert companies[0]["address"]["street"].name == "company[0].address.street"
        assert companies[0]["address"]["number"].name == "company[0].address.number"

        assert companies[1]["address"]["street"].value == street_2
        assert companies[1]["address"]["number"].value is None
        assert companies[1]["address"]["street"].name == "company[1].address.street"
        assert companies[1]["address"]["number"].name == "company[1].address.number"

        required_field_error = ErrorDetail(
            string="This field is required.", code="required"
        )
        assert companies[1]["address"]["number"].errors == [required_field_error]
