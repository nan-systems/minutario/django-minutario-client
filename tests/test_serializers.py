from django.http.request import QueryDict

from rest_framework import serializers
from rest_framework.exceptions import ErrorDetail

from django_minutario_client.nestling_serializers import NestlingSerializer
from django_minutario_client.serializers import (
    BrazilianAddressSerializer,
    DetailedBrazilianStateField,
    OrderedMultipleChoiceField,
)

required_field_error = ErrorDetail(string="This field is required.", code="required")


class TestBrazilianAddressSerializer:
    def test_correct_fields(self):
        serializer = BrazilianAddressSerializer()
        fields = (
            "cep",
            "logradouro",
            "numero",
            "complemento",
            "bairro",
            "cidade",
            "estado",
        )
        assert tuple(serializer.fields.keys()) == fields

    def test_correct_template(self):
        serializer = BrazilianAddressSerializer()
        default_style = dict(template="brazilian_address.html")
        assert default_style == serializer.style

    def test_override_template(self):
        style = dict(template="override.html")
        serializer = BrazilianAddressSerializer(style=style)
        assert style == serializer.style

    def test_validade_state_choices(self):
        data = dict(estado="42")
        serializer = BrazilianAddressSerializer(data=data)
        assert not serializer.is_valid()
        errors = serializer.errors
        assert "estado" in errors
        assert 1 == len(errors["estado"])
        assert "invalid_choice" == errors["estado"][0].code

    def test_not_required(self):
        serializer = BrazilianAddressSerializer(data={})
        assert serializer.is_valid()


class TestDetailedBrazilianStateField:
    def test_correct_fields(self):
        field = DetailedBrazilianStateField()
        state = field.to_internal_value("SP")
        assert "SP" == str(state)
        assert "SP" == state.sigla
        assert "São Paulo" == state.nome


class TestOrderedMultipleChoices:
    def test_correct_template(self):
        choices = []
        field = OrderedMultipleChoiceField(choices=choices)
        assert "template" in field.style
        assert "ordered_multiple_checkbox.html" == field.style["template"]

    def test_override_template(self):
        choices = []
        style = dict(template="override.html")
        field = OrderedMultipleChoiceField(choices=choices, style=style)
        assert style == field.style

    def test_one_value_when_not_nested(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        query_data = QueryDict(f"topics[]={topics[0]}")
        serializer = SectionSerializer(data=query_data)
        assert serializer.is_valid()
        assert [topics[0]] == serializer.data["topics"]

    def test_more_then_one_value_when_not_nested(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(NestlingSerializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        query_data = QueryDict(f"topics[]={topics[0]}&topics[]={topics[1]}")
        serializer = SectionSerializer(data=query_data)
        assert serializer.is_valid()
        assert topics == serializer.data["topics"]

    def test_validate_required_when_not_nested(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(serializers.Serializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        query_data = QueryDict()
        serializer = SectionSerializer(data=query_data)
        assert not serializer.is_valid()

        errors = {"topics": [required_field_error]}
        assert errors == serializer.errors

    def test_validate_not_required_when_not_nested(self):
        topics = ["topic 1", "topic 2"]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(serializers.Serializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES, required=False)

        query_data = QueryDict()
        serializer = SectionSerializer(data=query_data)
        assert serializer.is_valid()
        assert not serializer.errors

    def test_assure_order(self):
        from random import randint

        # we are using random values to try to prevent a false positive due set
        # hash
        topics = [randint(0, 100) for x in range(20)]
        TOPICS_CHOICES = list(zip(topics, topics))

        class SectionSerializer(serializers.Serializer):
            topics = OrderedMultipleChoiceField(choices=TOPICS_CHOICES)

        query_data = QueryDict(f"topics[]={topics[17]}&topics[]={topics[7]}")
        serializer = SectionSerializer(data=query_data)
        assert serializer.is_valid()
        assert [topics[7], topics[17]] == serializer.data["topics"]
