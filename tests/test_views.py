import datetime
import base64
import json
from decimal import Decimal
from unittest import mock

from django.conf import settings
from django.contrib import auth
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone
from rest_framework import serializers

import responses

from django_minutario_client.views import Minutario


User = auth.get_user_model()


class TestTemplateListView(TestCase):
    def setUp(self):
        self.url = reverse("template_list")
        self.minutario_endpoint = f"{settings.MINUTARIO_URL}/templates/"
        self.data = [
            {
                "pk": 1,
                "name": "Template name",
                "description": "Template description",
                "slug": "template",
                "template_versions": [{"name": "version name", "pk": 1}],
            }
        ]
        self.user = User.objects.create_user("user", password="password")
        self.client.login(username="user", password="password")

    @responses.activate
    def test_requires_authentication(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        self.client.logout()
        response = self.client.get(self.url)
        login_url = f"{settings.LOGIN_URL}?next={self.url}"

        self.assertRedirects(response, login_url, fetch_redirect_response=False)

    @responses.activate
    def test_correct_response(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        response = self.client.get(self.url)

        assert 200 == response.status_code
        self.assertTemplateUsed(
            response, template_name="django_minutario_client/template_list.html"
        )

    @responses.activate
    def test_correct_context_used(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        response = self.client.get(self.url)

        assert self.data == response.context["templates"]

    @responses.activate
    def test_correct_auth_used(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        with self.settings(MINUTARIO_AUTH=("test", "password")):
            self.client.get(self.url)

        encoded_auth = base64.b64encode(b"test:password").decode()
        auth_header = responses.calls[0].request.headers["authorization"]
        assert auth_header == f"Basic {encoded_auth}"


class TestTemplateDetailView(TestCase):
    def setUp(self):
        self.url = reverse("template_detail", kwargs={"slug": "template"})
        self.minutario_endpoint = f"{settings.MINUTARIO_URL}/documents/"
        self.data = [
            {
                "pk": 1,
                "template_version_info": [{"name": "version name", "pk": 1}],
            }
        ]

        self.paginated_data = {
            "count": 100,
            "next": "http://localhost:8000/templates/template/?limit=57&offset=57&test=?",
            "previous": "http://localhost:8000/templates/template/?offset=0",
            "results": [
                {
                    "pk": 2,
                    "template_version_info": {
                        "name": "name",
                        "name_template": "first template",
                        "description": "template test",
                    },
                    "data": {"wip": True},
                    "template_version": 1,
                    "filename": "first template",
                    "download_url": "http://localhost:8000/documents/2/download/",
                }
            ],
        }

        self.user = User.objects.create_user("user", password="password")
        self.client.login(username="user", password="password")

    @responses.activate
    def test_requires_authentication(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        self.client.logout()
        response = self.client.get(self.url)
        login_url = f"{settings.LOGIN_URL}?next={self.url}"

        self.assertRedirects(response, login_url, fetch_redirect_response=False)

    @responses.activate
    def test_correct_response(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        response = self.client.get(self.url)

        assert 200 == response.status_code
        self.assertTemplateUsed(
            response, template_name="django_minutario_client/template_detail.html"
        )

    @responses.activate
    def test_correct_context_used(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        response = self.client.get(self.url)

        assert self.data == response.context["documents"]
        assert "template" == response.context["slug"]

    @responses.activate
    def test_paginated_keys_do_not_show_up_in_context_when_page_size_is_not_set(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        response = self.client.get(self.url)

        assert self.data == response.context["documents"]
        assert "count" not in response.context
        assert "start" not in response.context
        assert "end" not in response.context
        assert "next" not in response.context
        assert "previous" not in response.context

    @responses.activate
    def test_correct_context_used_when_page_size_in_settings(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.paginated_data)
        minulito_host = "localhost"

        with self.settings(REST_FRAMEWORK={"PAGE_SIZE": 57}):
            response = self.client.get(self.url)
            assert self.paginated_data["results"] == response.context["documents"]
            assert self.paginated_data["count"] == response.context["count"]
            assert (
                "http://testserver/templates/template/?offset=0"
                == response.context["previous"]
            )
            assert (
                "http://testserver/templates/template/?limit=57&offset=57&test=%3F"
                == response.context["next"]
            )
            assert minulito_host not in response.context["next"]
            assert minulito_host not in response.context["previous"]
            assert 1 == response.context["start"]
            assert 57 == response.context["end"]

    @responses.activate
    def test_correct_auth_used(self):
        responses.add(responses.GET, self.minutario_endpoint, json=self.data)
        with self.settings(MINUTARIO_AUTH=("test", "password")):
            self.client.get(self.url)

        encoded_auth = base64.b64encode(b"test:password").decode()
        auth_header = responses.calls[0].request.headers["authorization"]
        assert auth_header == f"Basic {encoded_auth}"


class TestDocumentDownloadView(TestCase):
    def setUp(self):
        self.url = reverse("document_download", kwargs={"pk": 1})
        self.minutario_endpoint = f"{settings.MINUTARIO_URL}/documents/1/download/"
        self.data = b"Hello"

        self.user = User.objects.create_user("user", password="password")
        self.client.login(username="user", password="password")

    @responses.activate
    def test_requires_authentication(self):
        responses.add(responses.GET, self.minutario_endpoint, body=self.data)
        self.client.logout()
        response = self.client.get(self.url)
        login_url = f"{settings.LOGIN_URL}?next={self.url}"

        self.assertRedirects(response, login_url, fetch_redirect_response=False)

    @responses.activate
    def test_correct_response(self):
        responses.add(responses.GET, self.minutario_endpoint, body=self.data)
        response = self.client.get(self.url)

        assert 200 == response.status_code
        expected_data = [b for b in response.streaming_content][0]
        assert expected_data == self.data
        assert "document.docx" == response.filename

    @responses.activate
    def test_correct_auth_used(self):
        responses.add(responses.GET, self.minutario_endpoint, body=self.data)
        with self.settings(MINUTARIO_AUTH=("test", "password")):
            self.client.get(self.url)

        encoded_auth = base64.b64encode(b"test:password").decode()
        auth_header = responses.calls[0].request.headers["authorization"]
        assert auth_header == f"Basic {encoded_auth}"

    @responses.activate
    def test_correct_filename(self):
        responses.add(
            responses.GET,
            self.minutario_endpoint,
            body=self.data,
            adding_headers={
                "content-disposition": "attachment; filename=my_document.docx"
            },
        )
        response = self.client.get(self.url)

        assert 200 == response.status_code
        assert "my_document.docx" == response.filename

    @responses.activate
    def test_get_document_as_pdf(self):
        pdf_content = b"PDF content"
        responses.add(
            responses.GET,
            self.minutario_endpoint,
            body=pdf_content,
            adding_headers={
                "content-disposition": "attachment; filename=my_document.pdf"
            },
        )
        response = self.client.get(f"{self.url}?format=pdf")
        assert 200 == response.status_code
        assert "my_document.pdf" == response.filename
        assert responses.calls[0].request.url.endswith("format=pdf")

    @responses.activate
    def test_get_document_as_pdf_with_correct_extension_on_filename(self):
        pdf_content = b"PDF content"
        responses.add(
            responses.GET,
            self.minutario_endpoint,
            body=pdf_content,
            adding_headers={"content-disposition": "attachment;"},
        )
        response = self.client.get(f"{self.url}?format=pdf")
        assert 200 == response.status_code
        assert "document.pdf" == response.filename
        assert responses.calls[0].request.url.endswith("format=pdf")


class MockSerializer(serializers.Serializer):
    name = serializers.CharField()


@override_settings(DOCUMENT_SERIALIZER_CLASSES={"test": "test_views.MockSerializer"})
class TestDocumentCreateView(TestCase):
    def setUp(self):
        self.url = reverse("document_new", kwargs={"template_slug": "test"})
        self.documents_endpoint = f"{settings.MINUTARIO_URL}/documents/"
        self.templates_endpoint = f"{settings.MINUTARIO_URL}/templates/test/"
        self.template_data = {
            "pk": 1,
            "slug": "test",
            "template_versions": [
                {"pk": 1, "name": "first"},
                {"pk": 2, "name": "last"},
            ],
        }
        self.documents_data = [
            {
                "pk": 1,
                "template_version_info": [{"name": "version name", "pk": 2}],
            }
        ]

        self.user = User.objects.create_user("user", password="password")
        self.client.login(username="user", password="password")

    def test_requires_authentication(self):
        self.client.logout()
        response = self.client.get(self.url)
        login_url = f"{settings.LOGIN_URL}?next={self.url}"

        self.assertRedirects(response, login_url, fetch_redirect_response=False)

    def test_correct_response(self):
        response = self.client.get(self.url)
        assert 200 == response.status_code
        self.assertTemplateUsed("django_minutario_client/document_form.html")

    def test_correct_context(self):
        response = self.client.get(self.url)
        assert 200 == response.status_code
        assert isinstance(response.data["serializer"], MockSerializer), response.data
        assert response.data["template_slug"] == "test"

    @responses.activate
    def test_save_document_to_minutario(self):
        responses.add(responses.GET, self.templates_endpoint, json=self.template_data)
        responses.add(responses.POST, self.documents_endpoint, json=self.documents_data)
        response = self.client.post(self.url, {"name": "Thiago", "csrf": "blablabla"})

        self.assertRedirects(
            response,
            reverse("template_detail", kwargs={"slug": "test"}),
            fetch_redirect_response=False,
        )
        request = responses.calls[1].request

        expected_post_data = {
            "data": {"name": "Thiago"},
            "template_version": 2,
        }
        assert expected_post_data == json.loads(request.body)

    @mock.patch("django_minutario_client.views.Minutario")
    def test_invalid_data(self, Minutario):
        data_with_missing_value = {"csrf": "blablabla"}
        response = self.client.post(self.url, data_with_missing_value)
        assert 200 == response.status_code

        errors = response.data["serializer"].errors
        assert response.data["template_slug"] == "test"
        assert "name" in errors
        assert "required" == errors["name"][0].code


class TestMinutario(TestCase):
    def setUp(self):
        self.documents_endpoint = f"{settings.MINUTARIO_URL}/documents/"
        self.templates_endpoint = f"{settings.MINUTARIO_URL}/templates/test/"
        self.template_slug = "test"
        self.template_data = {
            "pk": 1,
            "slug": self.template_slug,
            "template_versions": [
                {"pk": 1, "name": "first"},
                {"pk": 2, "name": "last"},
            ],
        }
        self.documents_data = [
            {
                "pk": 1,
                "template_version_info": [{"name": "version name", "pk": 2}],
            }
        ]
        auth = ("username", "password")
        self.minutario = Minutario(auth)

    @responses.activate
    def test_encode_date(self):
        responses.add(responses.GET, self.templates_endpoint, json=self.template_data)
        responses.add(responses.POST, self.documents_endpoint, json=self.documents_data)

        self.minutario.create_document(
            self.template_slug, {"date": datetime.date(2020, 12, 4)}
        )

        request = responses.calls[1].request
        data = json.loads(request.body)["data"]

        assert data["date"] == "2020-12-04"

    @responses.activate
    def test_encode_decimal(self):
        responses.add(responses.GET, self.templates_endpoint, json=self.template_data)
        responses.add(responses.POST, self.documents_endpoint, json=self.documents_data)

        self.minutario.create_document(
            self.template_slug, {"decimal": Decimal("10.25")}
        )

        request = responses.calls[1].request
        data = json.loads(request.body)["data"]

        assert data["decimal"] == 10.25

    @responses.activate
    def test_encode_datetime_ware(self):
        responses.add(responses.GET, self.templates_endpoint, json=self.template_data)
        responses.add(responses.POST, self.documents_endpoint, json=self.documents_data)

        self.minutario.create_document(
            self.template_slug,
            {
                "datetime": datetime.datetime(
                    2020, 12, 4, 23, 45, 10, tzinfo=timezone.utc
                )
            },
        )

        request = responses.calls[1].request
        data = json.loads(request.body)["data"]

        self.assertEqual(data["datetime"], "2020-12-04T23:45:10Z")

    @responses.activate
    def test_return_created_document(self):
        responses.add(responses.GET, self.templates_endpoint, json=self.template_data)
        responses.add(responses.POST, self.documents_endpoint, json=self.documents_data)

        document = self.minutario.create_document(
            self.template_slug, {"name": "Thiago"}
        )

        assert self.documents_data == document
